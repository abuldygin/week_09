package ru.edu.task4.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class MainContainer {

    private SomeInterface someInterface;

    public MainContainer(SomeInterface someInterfaceBean) {
        someInterface = someInterfaceBean;
    }

    public SomeInterface getSomeInterface() {
        return someInterface;
    }
}
