package ru.edu.task2.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
public class AppJava {

    public static MainContainer run(){
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }
}
