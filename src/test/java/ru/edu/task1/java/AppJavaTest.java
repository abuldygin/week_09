package ru.edu.task1.java;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppJavaTest {

    @Test
    public void run() {
        assertTrue(AppJava.run().isValid());
    }
}